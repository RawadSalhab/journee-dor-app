import {NgModule, ErrorHandler} from "@angular/core";
import {IonicModule, IonicErrorHandler, IonicApp} from "ionic-angular";
import {MyApp} from "./app.component";
import {AboutPage} from "../pages/about/about";
import {HomePage} from "../pages/home/home";
import {TabsPage} from "../pages/tabs/tabs";
import {LoginPage} from "../pages/login/login";
import {RegisterPage} from "../pages/register/register";
import {CashBoxPage} from "../pages/cashbox/cashbox";
import {LogPage} from "../pages/logpage/logpage";
import {CfaPage} from "../pages/cfapage/cfapage";
import {CtdPage} from "../pages/ctdpage/ctdpage";
import {CibPage} from "../pages/cibpage/cibpage";
import {DataListPage} from "../pages/datalist/datalist"; //
import {PagesComponent} from "../components/pages/pages";
import {API} from "../providers/api";
import {Auth} from "../providers/auth";
import {CloudSettings, CloudModule} from "@ionic/cloud-angular";
import {Favorites} from "../providers/favorites";
import {ProfilePage} from "../pages/profile/profile";
import {FavoritesPage} from "../pages/favorites/favorites";
import {BrowserModule} from "@angular/platform-browser";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import {HttpModule} from "@angular/http";
import {Ahttp} from "../providers/Ahttp";
import {CurrencyPipe} from "@angular/common";
const cloudSettings: CloudSettings = {
    'core': {
        'app_id': '10919621'
    }
};

@NgModule({
    declarations: [
        MyApp,
        AboutPage,
        HomePage,
        TabsPage,
        LoginPage,
        RegisterPage,
        CashBoxPage,
        CfaPage,
        CibPage,
        CtdPage,
        LogPage,
        PagesComponent,
        ProfilePage,
        FavoritesPage,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(MyApp),
        CloudModule.forRoot(cloudSettings)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        AboutPage,
        HomePage,
        TabsPage,
        LoginPage,
        RegisterPage,
        CashBoxPage,
        CfaPage,
        CibPage,
        CtdPage,
        LogPage,
        PagesComponent,
        ProfilePage,
        FavoritesPage,
    ],
    providers: [
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        StatusBar,
        SplashScreen,
        API,
        Auth,
        Favorites,
        Ahttp,
        CurrencyPipe,
    ]
})
export class AppModule {
}


