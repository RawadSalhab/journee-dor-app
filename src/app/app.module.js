"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var app_component_1 = require('./app.component');
var about_1 = require('../pages/about/about');
var contact_1 = require('../pages/contact/contact');
var home_1 = require('../pages/home/home');
var tabs_1 = require('../pages/tabs/tabs');
var login_1 = require('../pages/login/login');
var register_1 = require('../pages/register/register');
var cashbox_1 = require('../pages/cashbox/cashbox');
var cfapage_1 = require('../pages/cfapage/cfapage');
var ctdpage_1 = require('../pages/ctdpage/ctdpage');
var cibpage_1 = require('../pages/cibpage/cibpage');
var logpage_1 = require('../pages/logpage/logpage');
var datalist_1 = require("../pages/datalist/datalist"); //
var api_1 = require("../providers/api");
var auth_1 = require("../providers/auth");
var navv_1 = require("../providers/navv");
var cloud_angular_1 = require('@ionic/cloud-angular');
var storage_1 = require('@ionic/storage');
var cloudSettings = {
    'core': {
        'app_id': '10919621'
    }
};
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.MyApp,
                about_1.AboutPage,
                contact_1.ContactPage,
                home_1.HomePage,
                tabs_1.TabsPage,
                login_1.LoginPage,
                register_1.RegisterPage,
                cashbox_1.CashBoxPage,
                cfapage_1.CfaPage,
                cibpage_1.CibPage,
                ctdpage_1.CtdPage,
                logpage_1.LogPage,
                datalist_1.DataListPage,
            ],
            imports: [
                ionic_angular_1.IonicModule.forRoot(app_component_1.MyApp),
                cloud_angular_1.CloudModule.forRoot(cloudSettings)
            ],
            bootstrap: [ionic_angular_1.IonicApp],
            entryComponents: [
                app_component_1.MyApp,
                about_1.AboutPage,
                contact_1.ContactPage,
                home_1.HomePage,
                tabs_1.TabsPage,
                login_1.LoginPage,
                register_1.RegisterPage,
                cashbox_1.CashBoxPage,
                cfapage_1.CfaPage,
                cibpage_1.CibPage,
                ctdpage_1.CtdPage,
                logpage_1.LogPage,
                datalist_1.DataListPage,
            ],
            providers: [{ provide: core_1.ErrorHandler, useClass: ionic_angular_1.IonicErrorHandler }, storage_1.Storage, api_1.API, auth_1.Auth, navv_1.NAVV]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
