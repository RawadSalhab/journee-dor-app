import {Component} from '@angular/core';
import {Platform, AlertController} from 'ionic-angular';
import {StatusBar, Splashscreen} from 'ionic-native';
import {LoginPage} from '../pages/login/login';
import {Deploy} from '@ionic/cloud-angular';
import {Channel} from '../providers/channel';
/*
 import {PagesComponent} from  '../components/pages/pages';
 */

@Component({
    templateUrl: 'app.html',
    providers: [Channel]
})
export class MyApp {
    rootPage = LoginPage;


    constructor(platform: Platform, public deploy: Deploy, public alertCtrl: AlertController, public channel: Channel) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
            Splashscreen.hide();
            if (platform.is('cordova')) {
                this.deploy.channel = this.channel.getChannel();
                console.log(this.deploy.channel);
                this.deploy.check().then((hasUpdate: boolean) => {
                    if (hasUpdate) {
                        this.deploy.download().then(() => {
                            this.deploy.extract().then(() => {
                                let confirm = this.alertCtrl.create({
                                    title: 'Update Available',
                                    message: 'Do you wish to restart your application?',
                                    buttons: [
                                        {
                                            text: 'Disagree',
                                            handler: () => {

                                            }
                                        },
                                        {
                                            text: 'Agree',
                                            handler: () => {
                                                deploy.load();
                                            }
                                        }
                                    ]
                                });
                                confirm.present();
                            });
                        });
                    }
                });
            }

            ///
        });
    }

}
