import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/add/operator/map';

/*
 Generated class for the API provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class Favorites {
    favoritesChanged = new EventEmitter();

    constructor() {
    }

    onAdd(item) {
        var favs = this.get();
        if (favs) {
            favs.forEach((fav, i) => {
                if (!fav || ((fav.id) == item.id)) {
                    favs.splice(i, 1);
                }
            });
        } else {
            favs = new Array();
        }
        favs.push(item);
        this.set(favs);
    }

    onDelete(item) {
        var favs = this.get();
        favs.forEach((fav, i) => {
            console.log(fav);
            if (!fav || ((fav.id) == item.id)) {
                favs.splice(i, 1);
            }
        });
        this.set(favs);
    }

    parse(favs) {
        return JSON.stringify(favs);
    }

    get() {
        var favs = JSON.parse(window.localStorage.getItem('favorites'));
        if (favs && favs.lenth > 0) {
            return favs[0];
        }
        return favs;
    }

    set(favs) {
        window.localStorage.setItem('favorites', this.parse(favs));
        this.favoritesChanged.emit(favs);
    }

    isFaved(item) {
        var favs = this.get();
        var returnVal = false;
        if (favs && favs.length > 0) {
            favs.forEach((fav, i) => {
                if (fav && ((fav.id) == item.id)) {
                    returnVal = true;
                }
            });
        }
        return returnVal;
    }

    totalPrice() {
        let favs = this.get();
        let total = 0;
        for (let fav of favs) {
            total += fav.price;
        }
        return total;
    }
}
