import {Injectable} from '@angular/core';
import {HomePage} from '../pages/home/home';
import {CfaPage} from '../pages/cfapage/cfapage';
import {CtdPage} from '../pages/ctdpage/ctdpage';
import {CibPage} from '../pages/cibpage/cibpage';
import {LogPage} from '../pages/logpage/logpage';


@Injectable()
export class NAVV {
    constructor() {
    }

    readonly TYPE_HOME = 10; // Home page
    readonly TYPE_CFA = 20; // Categories with fixed advertisements
    readonly TYPE_LOG = 30; // List of Governments
    readonly TYPE_CTD = 40; // Close to CFA but each group of categories has a title
    readonly TYPE_CIB = 50; // Same as CFA but with information text

    getPage(type): any {
        if (type == this.TYPE_HOME) {
            return HomePage;
        }
        else if (type == this.TYPE_CFA) {
            return CfaPage;
        }
        else if (type == this.TYPE_CIB) {
            return CibPage
        }
        else if (type == this.TYPE_CTD) {
            return CtdPage;
        }
        else if (type == this.TYPE_LOG) {
            return LogPage;
        }
        else {
            console.log("Error Found : type : " + type)
        }
    }


}