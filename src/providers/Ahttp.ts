import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, RequestOptionsArgs} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Loading, ToastController} from "ionic-angular";
import {Auth} from "./auth";

@Injectable()
export class Ahttp {

    constructor(private http: Http, private auth: Auth, private toastCtrl: ToastController) {
    }

    post(url: string, body: any, options?: RequestOptionsArgs, loading: Loading = null): Observable<any> {
        if (options === null || options === undefined) {
            options = new RequestOptions();
        }
        if (options.headers === null || options.headers === undefined) {
            options.headers = new Headers();
        }
        if (options.headers.has("Authorization"))
            options.headers.delete("Authorization");
        if (options.headers.has("Content-Type"))
            options.headers.delete("Content-Type");

        options.headers.append('Content-Type', 'application/json');

        if (this.auth.isLoggedIn()) {
            options.headers.append('Authorization', 'Bearer ' + this.auth.getToken());
        }
        return this.http.post(url, body, options).map(data => {
            if (loading) {
                loading.dismiss();
            }
            return data.json();
        }).catch((error: any) => {
            if (error.status === 422) {
                let err = error.json();
                let errMsg = null;
                if (err instanceof Array && err[0].message) {
                    errMsg = err[0].message;
                } else {
                    if (err[Object.keys(err)[0]][0]) {
                        errMsg = err[Object.keys(err)[0]][0];
                    }
                }
                let toast = this.toastCtrl.create({
                    message: errMsg ? errMsg : 'Something wrong just happened...',
                    duration: 1500
                });
                toast.present();
            }
            if (loading) {
                loading.dismiss();
            }
            return Observable.throw(new Error(error));
        });

    }

}
