"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var Channel = (function () {
    function Channel(storage, deploy, platform) {
        this.storage = storage;
        this.deploy = deploy;
        this.platform = platform;
        this.current = 'production';
        this.get();
    }
    Channel.prototype.get = function () {
        var _this = this;
        this.getChannel().then(function (val) {
            if (!val) {
                _this.current = 'production';
            }
            else {
                _this.current = val;
            }
        });
    };
    Channel.prototype.set = function (channel) {
        var _this = this;
        this.storage.set('channel', channel).then(function () {
            _this.get();
        });
    };
    Channel.prototype.getChannel = function () {
        return this.storage.get('channel');
    };
    Channel.prototype.getInfo = function () {
        if (this.platform.is('cordova')) {
            return this.deploy.info();
        }
    };
    Channel = __decorate([
        core_1.Injectable()
    ], Channel);
    return Channel;
}());
exports.Channel = Channel;
