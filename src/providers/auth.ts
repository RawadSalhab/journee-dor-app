import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';

/*
 Generated class for the API provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class Auth {

    constructor() {

    }

    login(token) {
        window.localStorage.setItem('token', token);
    }

    isLoggedIn() {
        return Boolean(window.localStorage.getItem('token'))
    }

    logout() {
        window.localStorage.removeItem('token');
    }

    getToken() {
        return window.localStorage.getItem('token');
    }
}
