/**
 * Created by Admin on 1/24/2017.
 */
import {Injectable} from '@angular/core';
import {Deploy} from '@ionic/cloud-angular';
import {Platform} from 'ionic-angular';

@Injectable()
export class Channel {
    public current = 'production';

    constructor(public deploy: Deploy, public platform: Platform) {
        this.get();
    }

    get() {
        let chan = window.localStorage.getItem('channel');
        if (!chan) {
            this.current = 'production';
        } else {
            this.current = chan;
        }
        // this.getChannel().then((val) => {
        //     if (!val) {
        //         this.current = 'production';
        //     } else {
        //         this.current = val;
        //     }
        // });

    }

    set(channel) {
        window.localStorage.setItem('channel', channel);
        this.get();
        // this.storage.set('channel', channel).then(() => {
        //     this.get();
        // });
    }

    getChannel() {
        return window.localStorage.getItem('channel');
        // return this.storage.get('channel');
    }

    getInfo() {
        if (this.platform.is('cordova')) {
            return this.deploy.info();
        }
    }
}
