import {Injectable} from "@angular/core";
import {RequestOptionsArgs} from "@angular/http";
import "rxjs/add/operator/map";
import {Ahttp} from "./Ahttp";
import {Observable} from "rxjs";
import {Loading, LoadingController} from "ionic-angular";

@Injectable()
export class API {
    url;
    loading: Loading = null;
    showLoading = true;
    options: RequestOptionsArgs;

    constructor(private http: Ahttp, private loadingCtrl: LoadingController) {
        this.url = 'http://api.journeedor.com';

    }


    signUp(data) {
        return this.post(this.url + '/user/signup', data);

    }

    loginBackEnd(data) {
        return this.post(this.url + '/user/login', data);
    }

    getHomePage() {
        return this.post(this.url + '/pages/home', {});
    }

    getPage(id) {
        return this.post(this.url + '/pages/page', {id: id});
    }

    getProfile() {
        return this.post(this.url + '/user/profile', {});
    }

    post(url: string, body: any): Observable<any> {
        if (this.showLoading && !this.loading) {
            this.loading = this.loadingCtrl.create();
        }
        if (this.loading) {
            this.loading.present();
        }
        let options = this.options;
        let loading = this.loading;
        this.options = null;
        this.loading = null;
        this.showLoading = true;
        return this.http.post(url, body, options, loading);
    }
}
