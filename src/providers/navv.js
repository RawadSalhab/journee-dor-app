"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var home_1 = require('../pages/home/home');
var cfapage_1 = require('../pages/cfapage/cfapage');
var ctdpage_1 = require('../pages/ctdpage/ctdpage');
var cibpage_1 = require('../pages/cibpage/cibpage');
var logpage_1 = require('../pages/logpage/logpage');
var NAVV = (function () {
    function NAVV() {
        this.TYPE_HOME = 10; // Home page
        this.TYPE_CFA = 20; // Categories with fixed advertisements
        this.TYPE_LOG = 30; // List of Governments
        this.TYPE_CTD = 40; // Close to CFA but each group of categories has a title
        this.TYPE_CIB = 50; // Same as CFA but with information text
    }
    NAVV.prototype.getPage = function (type) {
        if (type == this.TYPE_HOME) {
            return home_1.HomePage;
        }
        else if (type == this.TYPE_CFA) {
            return cfapage_1.CfaPage;
        }
        else if (type == this.TYPE_CIB) {
            return cibpage_1.CibPage;
        }
        else if (type == this.TYPE_CTD) {
            return ctdpage_1.CtdPage;
        }
        else if (type == this.TYPE_LOG) {
            return logpage_1.LogPage;
        }
        else {
            console.log("Error Found : type : " + type);
        }
    };
    NAVV = __decorate([
        core_1.Injectable()
    ], NAVV);
    return NAVV;
}());
exports.NAVV = NAVV;
