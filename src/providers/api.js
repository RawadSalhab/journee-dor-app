"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
/*
 Generated class for the API provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
var API = (function () {
    function API(http) {
        this.http = http;
        this.url = 'http://api.journeedor.com';
    }
    API.prototype.load = function () {
        return this.http.get('https://randomuser.me/api/?results=10')
            .map(function (res) { return res.json(); });
    };
    ;
    API.prototype.signUp = function (data) {
        return this.http.post(this.url + '/user/signup', data)
            .map(function (res) { return res.json(); });
    };
    API.prototype.loginBackEnd = function (data) {
        return this.http.post(this.url + '/user/login', data)
            .map(function (res) { return res.json(); });
    };
    API.prototype.getHomePage = function () {
        return this.http.get(this.url + '/pages/home')
            .map(function (res) { return res.json(); });
    };
    API.prototype.getPage = function (id) {
        return this.http.post(this.url + '/pages/page', { id: id })
            .map(function (res) { return res.json(); });
    };
    API = __decorate([
        core_1.Injectable()
    ], API);
    return API;
}());
exports.API = API;
