import {Component, Input} from '@angular/core';

@Component({
    selector: 'pages',
    templateUrl: 'pages.html'
})
export class PagesComponent {

    @Input() data = [];
    public childrenWithImages = [];
    public childrenNoImages = [];
    navv;

    constructor() {
        console.log(this.data);
        for (var i in this.data) { //For each child, if it has a SRC, push it to with images array, if it doesn't have src, push it to no images array :D
            if ((typeof(this.data[i].src) != 'undefined') && (this.data[i].src != null)) {
                this.childrenWithImages.push(this.data[i]);
            } else {
                this.childrenNoImages.push(this.data[i]);
            }
        }
    }

}
