"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var tabs_1 = require("../tabs/tabs");
var cfapage_1 = require("../cfapage/cfapage"); //
var datalist_1 = require("../datalist/datalist"); //
var login_1 = require("../login/login");
var HomePage = (function () {
    function HomePage(navCtrl, api, auth, navv) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.api = api;
        this.auth = auth;
        this.navv = navv;
        //
        this.api.getHomePage().subscribe(function (data) {
            console.log(data);
            _this.page = data;
            _this.title = data.title;
            _this.children = data.children;
        });
    }
    HomePage.prototype.goToTabsPage = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.navCtrl.push(tabs_1.TabsPage);
    };
    HomePage.prototype.goToCfaPage = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.navCtrl.push(cfapage_1.CfaPage);
    };
    HomePage.prototype.goToDataPage = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.navCtrl.push(datalist_1.DataListPage);
    };
    HomePage.prototype.hasError = function () {
        return (typeof (this.error) != 'undefined' && this.error.length > 0);
    };
    HomePage.prototype.logout = function () {
        this.auth.logout();
        this.navCtrl.popToRoot();
        this.navCtrl.setRoot(login_1.LoginPage);
    };
    HomePage.prototype.goTo = function (type, id) {
        this.navCtrl.push(this.navv.getPage(type), { id: id });
    };
    HomePage = __decorate([
        core_1.Component({
            selector: 'pagehome',
            templateUrl: 'home.html',
        })
    ], HomePage);
    return HomePage;
}());
exports.HomePage = HomePage;
