import {Component, ReflectiveInjector} from '@angular/core';
import {NavController} from 'ionic-angular';
/*import {TabsPage} from "../tabs/tabs";
 import {CfaPage} from "../cfapage/cfapage"; //
 import {DataListPage} from "../datalist/datalist"; //*/
import {API} from "../../providers/api";
import {Auth} from "../../providers/auth";
import {LoginPage} from "../login/login";
import {NAVV} from "../../providers/navv";
import {ProfilePage} from "../profile/profile";

@Component({
    selector: 'pagehome',
    templateUrl: 'home.html',
})
export class HomePage {

    public page;
    public title;
    public error;
    public jsonInput;
    public children = [];
    public childrenWithImages = [];
    public childrenNoImages = [];
    navv;

    constructor(public navCtrl: NavController, public api: API, public auth: Auth) {
        var injector = ReflectiveInjector.resolveAndCreate([NAVV]);
        this.navv = injector.get(NAVV);
        //
        this.api.getHomePage().subscribe(data => {
            console.log(data);
            this.page = data;
            this.title = data.title;
            this.children = data.children;
            for (var i in this.children) { //For each child, if it has a SRC, push it to with images array, if it doesn't have src, push it to no images array :D
                if ((typeof(this.children[i].src) != 'undefined') && (this.children[i].src != null)) {
                    this.childrenWithImages.push(this.children[i]);
                } else {
                    this.childrenNoImages.push(this.children[i]);
                }
            }
        });

    }


    hasError() {
        return (typeof(this.error) != 'undefined' && this.error.length > 0);
    }

    logout() {
        this.auth.logout();
        this.navCtrl.popToRoot();
        this.navCtrl.setRoot(LoginPage);
    }

    goTo(type, id) {
        this.navCtrl.push(this.navv.getPage(type), {id: id});
    }

    goToProfile() {
        this.navCtrl.push(ProfilePage);
    }
}
