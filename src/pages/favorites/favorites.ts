import {Component, ReflectiveInjector, OnInit} from '@angular/core';
import {NavController, LoadingController, ToastController, AlertController} from "ionic-angular";
import {Auth} from "../../providers/auth";
import {Favorites} from "../../providers/favorites";
import {NAVV} from "../../providers/navv";


@Component({
    selector: 'pagefavorites',
    templateUrl: 'favorites.html',
})
export class FavoritesPage implements OnInit {
    navv;
    favs = [];
    childrenWithImages = [];
    childrenNoImages = [];


    constructor(public navCtrl: NavController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public auth: Auth, public fp: Favorites, private alertCtrl: AlertController) {
        var injector = ReflectiveInjector.resolveAndCreate([NAVV]);
        this.navv = injector.get(NAVV);
    }

    ngOnInit() {
        this.fp.favoritesChanged.subscribe((res) => {
            this.favs = res;
            this.parseFavs();
        });
    }

    ionViewDidEnter() {
        this.favs = this.fp.get();
        this.parseFavs();
    }

    parseFavs() {
        this.childrenNoImages = [];
        this.childrenWithImages = [];
        for (var i in this.favs) { //For each child, if it has a SRC, push it to with images array, if it doesn't have src, push it to no images array :D
            if ((typeof(this.favs[i].src) != 'undefined') && (this.favs[i].src != null)) {
                this.childrenWithImages.push(this.favs[i]);
            } else {
                this.childrenNoImages.push(this.favs[i]);
            }
        }
    }

    goTo(page) {
        if (page.type == 60) { // TYPE_IMAGE = 60
            let alert = this.alertCtrl.create({
                title: page.title,
                subTitle: '<img src="' + page.src + '" /> <p>' + page.description + '</p>',
                buttons: [{
                    text: this.getFavText(page), handler: () => {
                        if (!this.fp.isFaved(page)) {
                            this.fp.onAdd(page);
                        } else {
                            this.fp.onDelete(page);
                        }
                    }
                }, 'Dismiss']
            });
            alert.present();
        } else {
            this.navCtrl.push(this.navv.getPage(page.type), {id: page.id});
        }
    }

    getFavText(page) {
        if (this.fp.isFaved(page)) {
            return 'Remove From Favorites';
        }
        return 'Add To Favorites';
    }


}
