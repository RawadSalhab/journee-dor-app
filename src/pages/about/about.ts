import {Component} from "@angular/core";
import {Channel} from "../../providers/channel";
import {NavController, Platform} from "ionic-angular";

@Component({
    selector: 'page-about',
    templateUrl: 'about.html',
    providers: [Channel]
})
export class AboutPage {
    public version;
    public uuid;

    constructor(public navCtrl: NavController, public channel: Channel, public platform: Platform) {
        this.getInfo();
    }

    getInfo() {
        if (this.platform.is('cordova')) {
            this.channel.getInfo().then((info) => {
                this.version = info.binary_version;
                this.uuid = info.deploy_uuid.split('-')[0];
            });
        }
    }

    setStaging() {
        this.channel.set('staging');
    }

    setDev() {
        this.channel.set('dev');
    }

    setProd() {
        this.channel.set('production');
    }
}
