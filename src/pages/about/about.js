"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var channel_1 = require('../../providers/channel');
var AboutPage = (function () {
    function AboutPage(navCtrl, storage, channel, platform) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.channel = channel;
        this.platform = platform;
        this.getInfo();
    }
    AboutPage.prototype.getInfo = function () {
        var _this = this;
        if (this.platform.is('cordova')) {
            this.channel.getInfo().then(function (info) {
                _this.version = info.binary_version;
                _this.uuid = info.deploy_uuid.split('-')[0];
            });
        }
    };
    AboutPage.prototype.setStaging = function () {
        this.channel.set('staging');
    };
    AboutPage.prototype.setDev = function () {
        this.channel.set('dev');
    };
    AboutPage.prototype.setProd = function () {
        this.channel.set('production');
    };
    AboutPage = __decorate([
        core_1.Component({
            selector: 'page-about',
            templateUrl: 'about.html',
            providers: [channel_1.Channel]
        })
    ], AboutPage);
    return AboutPage;
}());
exports.AboutPage = AboutPage;
