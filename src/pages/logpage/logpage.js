"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var LogPage = (function () {
    function LogPage(navCtrl, navParams, api, navv) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.navv = navv;
        this.api.getPage(this.navParams.data.id).subscribe(function (data) {
            _this.title = data.title;
            _this.children = data.children;
        });
    }
    LogPage.prototype.goTo = function (type, id) {
        this.navCtrl.push(this.navv.getPage(type), { id: id });
    };
    LogPage = __decorate([
        core_1.Component({
            selector: 'pagelog',
            templateUrl: 'logpage.html',
        })
    ], LogPage);
    return LogPage;
}());
exports.LogPage = LogPage;
