import {Component, ReflectiveInjector} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {API} from "../../providers/api";
import {NAVV} from "../../providers/navv";

@Component({
    selector: 'pagelog',
    templateUrl: 'logpage.html',
})
export class LogPage {
    title;
    children;
    navv;

    constructor(public navCtrl: NavController, public navParams: NavParams, public api: API) {
        var injector = ReflectiveInjector.resolveAndCreate([NAVV]);
        this.navv = injector.get(NAVV);
        this.api.getPage(this.navParams.data.id).subscribe(data => {
            this.title = data.title;
            this.children = data.children;
        });
    }

    goTo(type, id) {
        this.navCtrl.push(this.navv.getPage(type), {id: id});
    }

}
