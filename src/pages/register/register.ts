import {Component} from '@angular/core';
import {TabsPage} from "../tabs/tabs";
import {NavController, LoadingController, ToastController} from "ionic-angular";
import {API} from "../../providers/api";
import {Auth} from "../../providers/auth";


@Component({
    selector: 'pageregister',
    templateUrl: 'register.html',
})
export class RegisterPage {
    signup = {phone: "+961 ", password: '', password2: '', firstname: '', lastname: '', email: ''};


    constructor(public navCtrl: NavController, public api: API, public toastCtrl: ToastController, public auth: Auth) {
        //this.signup.phone = "961";
    }

    goToHomePage() {
        this.navCtrl.push(TabsPage);
    }

    phoneValidate(newValue, element) {
        setTimeout(() => {
            // console.log(this.signupForm);
            var noplus = newValue.substring(1);
            newValue = '+' + noplus.replace(/[^0-9\s]/, '');
            this.signup.phone = newValue;

            var trimmed = newValue.replace(/\s+/g, '');
            if (!newValue.startsWith("+961 ")) {
                this.signup.phone = "+961 ";
            }
            if (newValue.startsWith("+961 03")) {
                this.signup.phone = "+961 3";
            }
            if (newValue.startsWith("+961 06")) {
                this.signup.phone = "+961 6"
            }
            var maxlength = 12;
            if (trimmed.startsWith("+9616") || trimmed.startsWith("+9613")) {
                maxlength = 11;
            }
            if (trimmed.length > maxlength) {
                this.signup.phone = newValue.substr(0, newValue.length - (trimmed.length - maxlength));
            }
            this.verifyPhone(this.signup.phone, element._control);
        }, 0);
    }

    submit(model) {
        console.log(model);
        console.log(this.signup);

        this.verifyPhone(model.value.phone, model.controls.phone)
        this.api.signUp(this.signup).subscribe(data => {
            this.auth.login(data.token);
            this.navCtrl.popToRoot();
            this.navCtrl.setRoot(TabsPage);
        }, error => {
            console.log(error);
            var errorObj = JSON.parse(error._body);
            for (var firstError in errorObj.error) {
                model.controls.email.setErrors({valid: false});
                model.controls[firstError].setErrors({valid: false});
                let toast = this.toastCtrl.create({
                    message: errorObj.error[firstError],
                    duration: 3000
                });
                toast.present();
                return;
            }
        });
    }

    verifyPhone(value, control) {
        var trimmed = value.replace(/\s+/g, '');
        var minLength = 12;
        if (trimmed.startsWith("+9616") || trimmed.startsWith("+9613")) {
            minLength = 11;
        }
        if (trimmed.length < minLength) {
            control.setErrors({valid: false});
        }
    }

    passwordValidate(element) {
        setTimeout(() => {
            if (this.signup.password != this.signup.password2) {
                element.controls.password2.setErrors({valid: false});
            }
        }, 0);
    }
}
