"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var cibpage_1 = require('../cibpage/cibpage');
var tabs_1 = require('../tabs/tabs');
/*
 Generated class for the CtdPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var CtdPage = (function () {
    function CtdPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CtdPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CtdPage');
    };
    CtdPage.prototype.goToCibPage = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.navCtrl.push(cibpage_1.CibPage);
    };
    CtdPage.prototype.goToTabsPage = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.navCtrl.push(tabs_1.TabsPage);
    };
    CtdPage = __decorate([
        core_1.Component({
            selector: 'pagectdpage',
            templateUrl: 'ctdpage.html'
        })
    ], CtdPage);
    return CtdPage;
}());
exports.CtdPage = CtdPage;
