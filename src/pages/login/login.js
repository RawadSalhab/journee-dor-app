"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var register_1 = require("../register/register");
var tabs_1 = require("../tabs/tabs");
var LoginPage = (function () {
    function LoginPage(navCtrl, api, toastCtrl, auth, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.login = { email: '', password: '' };
        if (this.auth.isLoggedIn()) {
            // this.navCtrl.popToRoot();
            this.navCtrl.setRoot(tabs_1.TabsPage);
        }
    }
    LoginPage.prototype.goToTabsPage = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.navCtrl.push(tabs_1.TabsPage);
    };
    LoginPage.prototype.goToRegisterPage = function () {
        this.navCtrl.push(register_1.RegisterPage);
    };
    /*itemTapped(event, item) {
     this.navCtrl.push(LoginPage, {
     item: item
     });
     }*/
    LoginPage.prototype.loginSend = function (model) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.api.loginBackEnd(this.login).subscribe(function (data) {
            _this.auth.login(data.access_token);
            _this.navCtrl.popToRoot();
            _this.navCtrl.setRoot(tabs_1.TabsPage);
            loading.dismiss();
        }, function (error) {
            var errorObj = JSON.parse(error._body);
            for (var i = 0; i < errorObj.length; i++) {
                model.controls[errorObj[i].field].setErrors({ valid: false });
                var toast = _this.toastCtrl.create({
                    message: errorObj[i].message,
                    duration: 3000
                });
                toast.present();
                break;
            }
            loading.dismiss();
            return true;
        });
    };
    LoginPage = __decorate([
        core_1.Component({
            selector: 'pagelogin',
            templateUrl: 'login.html',
        })
    ], LoginPage);
    return LoginPage;
}());
exports.LoginPage = LoginPage;
