import {Component} from "@angular/core";
import {NavController, ToastController} from "ionic-angular";
import {RegisterPage} from "../register/register";
import {TabsPage} from "../tabs/tabs";
import {API} from "../../providers/api";
import {Auth} from "../../providers/auth";


@Component({
    selector: 'pagelogin',
    templateUrl: 'login.html',
})
export class LoginPage {
    login = {email: '', password: ''};

    constructor(public navCtrl: NavController, public api: API, public toastCtrl: ToastController, public auth: Auth) {
        if (this.auth.isLoggedIn()) {
            // this.navCtrl.popToRoot();
            this.navCtrl.setRoot(TabsPage);
        }
    }

    goToTabsPage() {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.navCtrl.push(TabsPage);
    }

    goToRegisterPage() {
        this.navCtrl.push(RegisterPage);
    }

    /*itemTapped(event, item) {
     this.navCtrl.push(LoginPage, {
     item: item
     });
     }*/


    loginSend(model) {


        this.api.loginBackEnd(this.login).subscribe(data => {
            this.auth.login(data.access_token);
            this.navCtrl.popToRoot();
            this.navCtrl.setRoot(TabsPage);

        }, error => {
            var errorObj = JSON.parse(error._body);
            for (var i = 0; i < errorObj.length; i++) {
                model.controls[errorObj[i].field].setErrors({valid: false});
                let toast = this.toastCtrl.create({
                    message: errorObj[i].message,
                    duration: 3000
                });
                toast.present();
                break;
            }
            return true;
        });

    }
}


