import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
/*
 Generated class for the CashBoxPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'pagecashbox',
    templateUrl: 'cashbox.html'
})
export class CashBoxPage {

    constructor(public navCtrl: NavController) {

    }

}
