import {Component, ReflectiveInjector} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {API} from "../../providers/api";
import {NAVV} from "../../providers/navv";
import {Auth} from "../../providers/auth";
// import {LoginPage} from "../login/login";

@Component({
    selector: 'pagehome',
    templateUrl: 'cfapage.html',

})
export class CfaPage {
    public page;
    title;
    children;
    navv;
    public childrenWithImages = [];
    public childrenNoImages = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public api: API, public auth: Auth) {
        var injector = ReflectiveInjector.resolveAndCreate([NAVV]);
        this.navv = injector.get(NAVV);
        this.api.getPage(this.navParams.data.id).subscribe(data => {
            console.log(data);
            this.page = data;
            this.title = data.title;
            this.children = data.children;
            for (var i in this.children) { //For each child, if it has a SRC, push it to with images array, if it doesn't have src, push it to no images array :D
                if ((typeof(this.children[i].src) != 'undefined') && (this.children[i].src != null)) {
                    this.childrenWithImages.push(this.children[i]);
                } else {
                    this.childrenNoImages.push(this.children[i]);
                }
            }
        });
    }

    goTo(type, id) {
        this.navCtrl.push(this.navv.getPage(type), {id: id});
    }

}
