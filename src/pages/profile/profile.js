"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var tabs_1 = require("../tabs/tabs");
var RegisterPage = (function () {
    function RegisterPage(navCtrl, api, toastCtrl, auth, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.signup = { phone: "+961 ", password: '', password2: '', firstname: '', lastname: '', email: '' };
        //this.signup.phone = "961";
    }
    RegisterPage.prototype.goToHomePage = function () {
        this.navCtrl.push(tabs_1.TabsPage);
    };
    RegisterPage.prototype.phoneValidate = function (newValue, element) {
        var _this = this;
        setTimeout(function () {
            // console.log(this.signupForm);
            var noplus = newValue.substring(1);
            newValue = '+' + noplus.replace(/[^0-9\s]/, '');
            _this.signup.phone = newValue;
            var trimmed = newValue.replace(/\s+/g, '');
            if (!newValue.startsWith("+961 ")) {
                _this.signup.phone = "+961 ";
            }
            if (newValue.startsWith("+961 03")) {
                _this.signup.phone = "+961 3";
            }
            if (newValue.startsWith("+961 06")) {
                _this.signup.phone = "+961 6";
            }
            var maxlength = 12;
            if (trimmed.startsWith("+9616") || trimmed.startsWith("+9613")) {
                maxlength = 11;
            }
            if (trimmed.length > maxlength) {
                _this.signup.phone = newValue.substr(0, newValue.length - (trimmed.length - maxlength));
            }
            _this.verifyPhone(_this.signup.phone, element._control);
        }, 0);
    };
    RegisterPage.prototype.submit = function (model) {
        var _this = this;
        console.log(model);
        console.log(this.signup);
        var loading = this.loadingCtrl.create();
        loading.present();
        this.verifyPhone(model.value.phone, model.controls.phone);
        this.api.signUp(this.signup).subscribe(function (data) {
            _this.auth.login(data.token);
            _this.navCtrl.popToRoot();
            _this.navCtrl.setRoot(tabs_1.TabsPage);
            loading.dismiss();
        }, function (error) {
            console.log(error);
            var errorObj = JSON.parse(error._body);
            for (var firstError in errorObj.error) {
                model.controls.email.setErrors({ valid: false });
                model.controls[firstError].setErrors({ valid: false });
                var toast = _this.toastCtrl.create({
                    message: errorObj.error[firstError],
                    duration: 3000
                });
                loading.dismiss();
                toast.present();
                return;
            }
        });
    };
    RegisterPage.prototype.verifyPhone = function (value, control) {
        var trimmed = value.replace(/\s+/g, '');
        var minLength = 12;
        if (trimmed.startsWith("+9616") || trimmed.startsWith("+9613")) {
            minLength = 11;
        }
        if (trimmed.length < minLength) {
            control.setErrors({ valid: false });
        }
    };
    RegisterPage.prototype.passwordValidate = function (element) {
        var _this = this;
        setTimeout(function () {
            if (_this.signup.password != _this.signup.password2) {
                element.controls.password2.setErrors({ valid: false });
            }
        }, 0);
    };
    RegisterPage = __decorate([
        core_1.Component({
            selector: 'pageregister',
            templateUrl: 'register.html',
        })
    ], RegisterPage);
    return RegisterPage;
}());
exports.RegisterPage = RegisterPage;
