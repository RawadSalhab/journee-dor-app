import {Component} from "@angular/core";
import {NavController, ToastController, LoadingController} from "ionic-angular";
import {Auth} from "../../providers/auth";
import {LoginPage} from "../login/login";
import {API} from "../../providers/api";


@Component({
    selector: 'pageprofile',
    templateUrl: 'profile.html',
})
export class ProfilePage {
    profile: any;


    constructor(public navCtrl: NavController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public auth: Auth, private api: API) {

    }

    ionViewDidLoad() {
        this.api.getProfile().subscribe((res) => {
            this.profile = res.user;
        })
    }

    logout() {
        this.auth.logout();
        this.navCtrl.popToRoot();
        this.navCtrl.setRoot(LoginPage);
    }


}
