import {Component, ReflectiveInjector} from '@angular/core';
import {NavController, NavParams, AlertController} from 'ionic-angular';
import {NAVV} from "../../providers/navv";
import {API} from "../../providers/api";
import {Auth} from "../../providers/auth";
import {Favorites} from "../../providers/favorites";
import {CurrencyPipe} from "@angular/common";

/*
 Generated class for the CibPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'cibpage',
    templateUrl: 'cibpage.html'
})
export class CibPage {
    title;
    children;
    navv;
    public page;
    public childrenWithImages = [];
    public childrenNoImages = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public api: API, public auth: Auth, private alertCtrl: AlertController, public favs: Favorites, private currency: CurrencyPipe) {
        var injector = ReflectiveInjector.resolveAndCreate([NAVV]);
        this.navv = injector.get(NAVV);
        this.api.getPage(this.navParams.data.id).subscribe(data => {
            console.log(data);
            this.title = data.title;
            this.children = data.children;
            this.page = data;
            for (var i in this.children) { //For each child, if it has a SRC, push it to with images array, if it doesn't have src, push it to no images array :D
                if ((typeof(this.children[i].src) != 'undefined') && (this.children[i].src != null)) {
                    this.childrenWithImages.push(this.children[i]);
                } else {
                    this.childrenNoImages.push(this.children[i]);
                }
            }
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CibPage');
    }

    goToCibPage() {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.navCtrl.push(CibPage);
    }

    goTo(page) {
        if (page.type == 60) { // TYPE_IMAGE = 60
            let alert = this.alertCtrl.create({
                title: page.title,
                subTitle: '<img src="' + page.src + '" /> <p>' + page.description + '</p>' + '<p>' + this.currency.transform(page.price, 'USD') + '</p>',
                buttons: [{
                    text: this.getFavText(page), handler: () => {
                        if (!this.favs.isFaved(page)) {
                            this.favs.onAdd(page);
                        } else {
                            this.favs.onDelete(page);
                        }
                    }
                }, 'Dismiss']
            });
            alert.present();
        } else {
            this.navCtrl.push(this.navv.getPage(page.type), {id: page.id});
        }
    }

    getFavText(page) {
        if (this.favs.isFaved(page)) {
            return 'Remove From Favorites';
        }
        return 'Add To Favorites';
    }
}
